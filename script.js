const session = new URL(location.href).searchParams.get('session')

let classes = {}
let classeSelectionnee = null
let appelMode = false
let absents = []
const importGroupFile = document.getElementById('import-groups-file')
if (importGroupFile !== null) importGroupFile.onchange = (event) => { openFileGroups(event) }
const importGroupField = document.getElementById('import-groups-text').onchange = (event) => {
    dataGroupAnalyser(event.target.value)
}

function openFileGroups(event) {
    // le fichier est un txt avec lignes au format Numero\tNom[\tclasse] ou Nom seul
    if (event.target !== null) {
        const input = event.target
        const reader = new FileReader()
        reader.onload = function () {
            if (typeof reader.result === 'string') {
                const result = reader.result
                dataGroupAnalyser(result)
            }
        }
        if (input.files !== null) {
            const file = input.files[0]
            reader.readAsText(file)
        }
    }
}

function testIfSexe(str) {
    if (['féminin', 'feminin', 'fille'].includes(str.toLowerCase())) {
        return 'f'
    }
    if (['masculin', 'homme', 'gars', 'garçon', 'garcon'].includes(str.toLowerCase())) {
        return 'm'
    }
    return str
}

function dataGroupAnalyser(result) {
    if (confirm('Ok pour remplacer les classes\nAnnuler sinon.')) {
        // afficher les données d'une ligne pour effectuer les rapprochements de champs
        const selectIndexes = { 1: 'prenom', 2: 'nom', 3: 'classe', 4: 'sexe' }
        const correspondances = {}
        // prendre la première ligne
        const lines = result.split(/[\r\n]+/g)
        const line0 = lines[0].split(/[\t;,]/g)
        const line1 = lines[1].split(/[\t;,]/g)
        const selectContent = `
<option value=''>Choix</option>
<option>Prénom</option>
<option>NOM</option>
<option>Classe</option>
<option>Sexe</option>
`
        const domImport = document.getElementById('import-1st')
        const table = document.createElement('table')
        const thead = document.createElement('thead')
        table.appendChild(thead)
        thead.innerHTML = `
<tr>
<td><input type="checkbox" id="ignorel0" value="true" title="ignorer"></td>
<td> <input type="checkbox" id="ignorel1" value="true" title="ignorer"> </td>
<td><- Ignorer</td>
<tr>
<th>Ligne 1</th>
<th>Ligne 2</th>
<th>Corresp.</th></tr>`
        const tbody = document.createElement('tbody')
        table.appendChild(tbody)
        for (const [index, txt] of line0.entries()) {
            const tr = document.createElement('tr')
            const td0 = document.createElement('td')
            td0.innerText = txt
            const td1 = document.createElement('td')
            td1.innerText = line1[index]
            const td2 = document.createElement('td')
            const select = document.createElement('select')
            select.id = 'selectCorresp' + index
            select.innerHTML = selectContent
            select.onchange = () => {
                if (select.selectedIndex > 0) {
                    select.classList.add('green')
                    correspondances[index] = select.selectedIndex
                }
                else {
                    select.classList.remove('green')
                    delete correspondances[index]
                }
            }
            td2.appendChild(select)
            tr.appendChild(td0)
            tr.appendChild(td1)
            tr.appendChild(td2)
            tbody.appendChild(tr)
        }
        domImport.appendChild(table)
        const newBtn = document.createElement('button')
        newBtn.type = 'submit'
        newBtn.classList.add('button', 'is-success')
        newBtn.id = 'btn-import-students'
        newBtn.innerText = 'Valider'
        document.getElementById('modal-importStudents-footer').prepend(newBtn)
        newBtn.onclick = (evt) => {
            evt.preventDefault()
            // effacement des données de la base
            for (const i in classes) {
                window.localStorage.removeItem('plan' + i)                
            }
            // on vide la table des élèves
            classes = {};
            console.log('Table des élèves vidée.')
            for (const [index, line] of lines.entries()) {
                if (document.getElementById('ignorel0').checked && index === 0) continue;
                if (document.getElementById('ignorel1').checked && index === 1) continue;
                if (line === '') continue;
                const values = line.split(/[\t;,]/g)
                const newUser = {}
                for (const [ind, value] of values.entries()) {
                    if (correspondances[ind] !== undefined) {
                        newUser[selectIndexes[correspondances[ind]]] = testIfSexe(value.replaceAll('"', '').trim())
                    }
                }
                if (classes[newUser.classe] === undefined) {
                    classes[newUser.classe] = []
                }
                classes[newUser.classe].push(newUser)
            }
            window.localStorage.setItem('classes',JSON.stringify(classes))
            console.log(classes)
            generateSelectClasse(classes)
            closeModal(document.getElementById('modal-addVariousStudents'))
        }
    }
}

// function to close modal 
function closeModal($el) {
    if ($el === null) return
    $el.classList.remove('is-active');
}

function generateSelectClasse(classes){
    const selectClasse = document.getElementById('select-classes')
    selectClasse.innerHTML = ''
    const optionChoice = document.createElement('option')
    optionChoice.innerText = 'Sélectionner la Classe'
    selectClasse.appendChild(optionChoice)
    for(const classe in classes){
        const option = document.createElement('option')
        option.innerText = classe
        option.value = classe
        selectClasse.appendChild(option)
    }
}
function shuffleArray(array) {
    let curId = array.length;
    // There remain elements to shuffle
    while (0 !== curId) {
      // Pick a remaining element
      let randId = Math.floor(Math.random() * curId);
      curId -= 1;
      // Swap it with the current element.
      let tmp = array[curId];
      array[curId] = array[randId];
      array[randId] = tmp;
    }
    return array;
  }

function placeEleves(classeName, force = false){
    absents = []
    tirages = []
    const fillegars = document.getElementById('fillegars').checked
    const localPlan = localStorage.getItem('plan'+classeName)
    classeSelectionnee = classeName
    if(localPlan !== null && !force){
        document.getElementById('svg5').outerHTML = localPlan
        refreshSVG()
    } else {
        const emplacementsEleves = Array.from(document.querySelectorAll('#svg5 text.draggable'))
        // trie le tableau selon l'ordre alpha des id
        emplacementsEleves.sort((a, b) => a.id.localeCompare(b.id))
        if(!fillegars){
            let places = Array.from({length: 30}, (_, i) => i)
            const eleves = shuffleArray(classes[classeName])
            let index = 0
            for (const [i,eleve] of eleves.entries()){
                afficheNom(emplacementsEleves, eleve, i)
                index++
            }
            for(let j=index+1;j<30;j++){
                emplacementsEleves[places[j]].innerHTML = '_'
            }
        } else {
            // compter combien de filles
            const nbFilles = classes[classeName].filter(eleve => eleve.sexe === 'f').length
            const nbGarcons = classes[classeName].filter(eleve => eleve.sexe === 'm').length
            const nbOfPairs = Math.min(nbFilles, nbGarcons)
            // on va placer autant de filles que de garçons
            const placesFilles = Array.from({length: nbOfPairs}, (_, i) => i*2)
            const placesGarcons = Array.from({length: nbOfPairs}, (_, i) => i*2+1)
            let filles = classes[classeName].filter(eleve => eleve.sexe === 'f')
            let garcons = classes[classeName].filter(eleve => eleve.sexe === 'm')
            // on détermine les places restantes une fois ces élèves placés
            let reste = []
            const nbPlacesPrises = placesFilles.length + placesGarcons.length
            for(let i=nbPlacesPrises;i<30;i++){
                reste.push(i)
            }
            filles = shuffleArray(filles)
            garcons = shuffleArray(garcons)
            let indexF = 0
            for (const [i,eleve] of placesFilles.entries()){
                afficheNom(emplacementsEleves, filles[i], eleve)
                indexF++
            }
            let indexG = 0
            for (const [i,eleve] of placesGarcons.entries()){
                afficheNom(emplacementsEleves, garcons[i], eleve)
                indexG++
            }
            for(let j = 0, len = reste.length; j<len; j++){
                if (filles[j+indexF] !== undefined) {
                    afficheNom(emplacementsEleves, filles[j+indexF], reste[j])
                } else if (garcons[j+indexG] !== undefined) {
                    afficheNom(emplacementsEleves, garcons[j+indexG], reste[j])
                } else {
                    emplacementsEleves[reste[j]].innerHTML = '_'
                }
            }
        }
        savePlan(classeName)
    }
}

function afficheNom (emplacementsEleves, eleve, index) {
    if (eleve === undefined) return
    emplacementsEleves[index].innerHTML = '';
    const tspanPrenom = document.createTextNode(eleve.prenom);
    emplacementsEleves[index].appendChild(tspanPrenom)
    const tspanNom =  document.createElementNS('http://www.w3.org/2000/svg','tspan');
    tspanNom.setAttribute('x',emplacementsEleves[index].attributes.x.value)
    tspanNom.setAttribute('dy','1em')
    tspanNom.setAttribute('font-size', '4')
    tspanNom.textContent = eleve.nom;
    emplacementsEleves[index].appendChild(tspanNom)
}

function savePlan(classeName){
    const svgElem = document.getElementById('svg5')
    const svgData = svgElem.outerHTML
    localStorage.setItem('plan'+classeName, svgData)
}
function uploadPlans(){
    if(session !== undefined) {
        const theclasses = {classes: classes, planbase: localStorage.getItem('planbase')}
        for(const classeName in classes){
            theclasses[classeName] = localStorage.getItem('plan'+classeName)
        }
        const data = new FormData();
        const jsonBlob = new Blob([theclasses], {type: "application/json"});
        data.append("data", jsonBlob);
        fetch('push.php', {
            method: "POST",
            body: data,
            }).then(resp => {
                if(!resp.ok) {
                    const err = new Error("Response wasn't okay");
                    err.resp = resp;
                    throw err;
                }
            console.log("Okay!");
        }).catch(err => {
            console.error(err);
        });
    }
}
function downloadImg() {
    const svgElem = document.getElementById('svg5')
    const svgData = svgElem.outerHTML
    const svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"})
    let DOMURL = window.URL || window.webkitURL || window;
    const url = DOMURL.createObjectURL(svgBlob);
    download(url);
  }
  
  function download(href) {
    let download = document.createElement('a');
    download.href = href;
    download.download = 'plan de la '+classeSelectionnee+'.svg';
    download.click();
    download.remove();
  }

function makeCliquable() {
    const thesvg = document.getElementById('svg5');
    let selectedElement = false;
    thesvg.addEventListener('mouseup', takeElement);
    thesvg.addEventListener('mouseleave', untakeElement);
    
    thesvg.addEventListener('touchend', takeElement);
    thesvg.addEventListener('touchleave', untakeElement);
    thesvg.addEventListener('touchcancel', untakeElement);
    function takeElement(evt){
        const elt = evt.target
        if(!elt.classList.contains('draggable')) return
        if(appelMode){
            elt.classList.toggle('abs')
            const elem = document.createElement('div')
            elem.innerHTML = elt.innerHTML
            const eleve = getEleveId(elem.childNodes[0].textContent, elem.childNodes[1].textContent)
            if(elt.classList.contains('abs')){
                elt.setAttribute('fill', 'grey')
                absents.push(eleve)
            } else {
                elt.removeAttribute('fill')
                absents = absents.filter(item => item !== eleve)
            }
            return;
        }
        if(elt.classList.contains('draggable') && !selectedElement){
            selectedElement = elt
        } else {

        }
    }
    function untakeElement(evt){
        selectedElement = false
    }
}
function refreshSVG() {
    makeCliquable()
}

function getEleveId (prenom, nom){
    const laclasse = classes[classeSelectionnee]
    for(const [index,eleve] of laclasse.entries()) {
        if(eleve.prenom == prenom && eleve.nom == nom) return index
    }
}

let clicked = false;
let tirages=[];
let setout = null
document.getElementById('letter').onclick = ()=>{
    if(!clicked){
        if(selectStudent()){
            document.getElementById('letter').classList.add('clicked')
            clicked = true
            setout = setTimeout(()=>{
                document.getElementById('letter').classList.remove('clicked')
                clicked = false
            }, 4000);}
    } else {
        document.getElementById('letter').classList.remove('clicked')
        clearTimeout(setout)
        setout = null
        clicked = false
    }
};

function selectStudent(){
	if(!classeSelectionnee){
		document.getElementById("choosenname").innerHTML = "";
		return false;
	}
	let len = classes[classeSelectionnee].length;
	if(len - absents.length === tirages.length){
		tirages = []
	};
	let number
	do {
		number = Math.floor(Math.random()*len);
	}
	while((tirages.indexOf(number)>-1 || absents.indexOf(number)>-1))
	tirages.push(number)
	document.getElementById("choosenname").innerHTML = classes[classeSelectionnee][number].prenom +'<br>'+ classes[classeSelectionnee][number].nom;
	return true
}


document.addEventListener('DOMContentLoaded', () => {
    // Add a click event on various child elements to close the parent modal
    (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
        const $target = $close.closest('.modal');

        $close.addEventListener('click', () => {
            closeModal($target);
        });
    });
    document.getElementById('colorTables').onchange = (evt) => {
        document.querySelectorAll('svg .tables').forEach(elt => {
            elt.attributes['fill'].value = evt.target.value
        })
        document.getElementById('svg5').innerHTML += ''
    }
    const btnImportStudents = document.getElementById('btn-import-students')
    btnImportStudents.onclick = () => {
        document.getElementById('modal-addVariousStudents').classList.add('is-active')
    }
    document.getElementById('select-classes').onchange = (evt) => {
        const classe = evt.target.selectedIndex
        if(classe > 0){
            placeEleves(evt.target.value)
        }
    }
    document.getElementById('btn-melange-selected-classe').onclick = () => {
        const selectClasse = document.getElementById('select-classes')
        if(selectClasse.selectedIndex > 0){
            placeEleves(selectClasse.value, true)
        }
    }
    const classesInStorage = localStorage.getItem('classes')
    if(classesInStorage !== null){
        classes = JSON.parse(classesInStorage)
        generateSelectClasse(classes)
    }
    savePlan('base')
    document.getElementById('btn-download').onclick = () => {downloadImg()}
    document.getElementById('btn-reset').onclick = () => {
        const localPlan = localStorage.getItem('planbase')
        if(localPlan !== null){
            document.getElementById('svg5').outerHTML = localPlan
            refreshSVG()
    }}
    document.getElementById('btn-save').onclick = () => {savePlan(classeSelectionnee)}
    document.getElementById('btn-appel').onclick = () => {appelMode = !appelMode; document.getElementById('btn-appel').classList.toggle('active')}
    document.getElementById('btn-interrogate').onclick = () => {
        appelMode = false
        document.getElementById('btn-appel').classList.remove('active')
        document.getElementById('modal-interroger').classList.toggle('is-active')
    }
    document.getElementById('tagall').onclick = (evt) => {
        const what = evt.target.innerText
        let action = 'remove'
        if (what === 'Marquer tout') {
            evt.target.innerText = 'Démarquer tout'
            action = 'add'
        } else {
            evt.target.innerText = 'Marquer tout'
        }
        document.querySelectorAll('#svg5 text.draggable').forEach(el => {
            if(action === 'remove'){
                el.classList.remove('vise')
                el.removeAttribute('fill')
            } else {
                el.classList.add('vise')
                el.setAttribute('fill', 'green')
            }
        })
    }
    refreshSVG()
})